//console.log("Hello World!");

//ARITHMETIC OPERATORS

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of sum: "+sum);

let difference = x - y;
console.log("Result of difference: "+difference);

let product = x * y;
console.log("Result of product: "+product);

let quotient = x / y;
console.log("Result of division: "+quotient);

// modulus % gets the remainder
let remainder = y % x;
console.log("Result of remainder: "+remainder);

// assignment operator =
let assignmentNumber = 8;
assignmentNumber += 2;
console.log("Result of addition assignment operator: "+assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: "+assignmentNumber);

assignmentNumber *= 2;
console.log("Result of product assignment operator: "+assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: "+assignmentNumber);

// multiple operators
let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of mdas operation: "+mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: "+pemdas);

//Incrementation vs Decrementation
// pre - immediate effect
//post - only z is affected
let z = 1;

let increment = ++z;
console.log("Result of pre-incrementation: "+increment);
console.log("Result of pre-incrementation: "+z);
increment = z++;
console.log("Result of post-incrementation: "+increment);
console.log("Result of post-incrementation: "+z);

let decrement = --z;
console.log("Result of pre-decrementation: "+ decrement);
console.log("Result of pre-decrementation: "+ z);
decrement = z--;
console.log("Result of post-decrementation: "+ decrement);
console.log("Result of post-decrementation: "+ z);

// type coercion
let numA = "10";
let numB = 12;
let coercion = numA + numB;
console.log(coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);

let numE = false +1;
console.log(numE);

// comparison operators

let juan = "juan";

// in/equality operator == !=
// checks 2 operands if they are equal /have the same content
// may return boolean value

console.log(1==1);

// stirct in/equality === !==

// relational operator
let a = 50;
let b = 65;

//>  <  >=  <=  
let isGreaterThan = a > b;

//NaN=not a number

//logical operators nd-&&  or-||  nor-!
let isLegalAge == true;
let isRegistered = false;

